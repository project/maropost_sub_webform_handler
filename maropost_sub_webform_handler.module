<?php

/**
 * @file
 * Primary module hooks for Maropost Subscription Webform Handler module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function maropost_sub_webform_handler_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.basic_auth':
      $output = '<h3>A simple Webform handler that allows site builders and developers to easily submit new leads to Maropost Subscriptions.</h3>';
      $output .= '<h4>How to use:</h4>';
      $output .= '<ol>';
      $output .= '  <li>Download and install the module via composer</li>';
      $output .= '  <li>Set your <b>API Auth Token</b> and <b>Maropost URL</b> in the configuration form: <pre>/admin/config/system/maropost-integration</pre></li>';
      $output .= '  <li>Edit the webform you want to create Maropost subscriptions from, and add a new Handler: <pre>/admin/structure/webform/manage/<your_webform_id>/handlers</pre></li>';
      $output .= '  <li>Choose "Maropost handler" and configure it like the following:';
      $output .= '    <ol>';
      $output .= '      <li>General settings: fill them as usual with relevant name and description; since you can use MORE than 1 handler, please be sure to use meaningful names</li>';
      $output .= '      <li>Specific settings - Subscription ID: the ID of the subscription list the lead will be added on</li>';
      $output .= '      <li>Specific settings - Email token: the token value that will be replaced with the actual subscription mail (i.e. <pre>[webform_submission:values:field_email]</pre>)</li>';
      $output .= '      <li>Specific settings - First name token: the token value that will be replaced with lead first name</li>';
      $output .= '      <li>Specific settings - Last name token: the token value that will be replaced with lead last name</li>';
      $output .= '      <li>Specific settings - Custom data token: a list of keys|token pairs (one per row, separated by <pre>|</pre>) that will be used as "Custom data" entries.</li>';
      $output .= '  </li>';
      $output .= '</ol>';
      $output .= '<p>That\'s it: every time a user submits your webform successfully, the lead information are sent to the corresponding Maropost List via the configured credentials.</p>';
      $output .= '<p>The Plugin comes with a switchable "Debug" mode, to analyze the submission results with ease.</p>';
      return $output;
  }
}
