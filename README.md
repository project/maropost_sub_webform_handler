# A simple Webform handler that allows site builders and developers to easily submit new leads to Maropost Subscriptions.

## How to use:
* Download and install the module via composer
* Set your API Auth Token and Maropost URL in the configuration form: `/admin/config/system/maropost-integration`
* Edit the webform you want to create Maropost subscriptions from, and add a new Handler: `/admin/structure/webform/manage/<your_webform_id>/handlers`
* Choose "Maropost handler" and configure it like the following:
  * General settings: fill them as usual with relevant name and description; since you can use MORE than 1 handler, please be sure to use meaningful names
  * Specific settings - Subscription ID: the ID of the subscription list the lead will be added on
  * Specific settings - Email token: the token value that will be replaced with the actual subscription mail (i.e. `[webform_submission:values:field_email]`)
  * Specific settings - First name token: the token value that will be replaced with lead first name
  * Specific settings - Last name token: the token value that will be replaced with lead last name
  * Specific settings - Custom data token: a list of keys|token pairs (one per row, separated by `|`) that will be used as "Custom data" entries.
That's it: every time a user submits your webform successfully, the lead information are sent to the corresponding Maropost List via the configured credentials.

The Plugin comes with a switchable "Debug" mode, to analyze the submission results with ease.

