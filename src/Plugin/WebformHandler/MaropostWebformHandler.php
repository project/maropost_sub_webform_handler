<?php

namespace Drupal\maropost_sub_webform_handler\Plugin\WebformHandler;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Webform example handler.
 *
 * @WebformHandler(
 *   id = "maropost",
 *   label = @Translation("Maropost Handler"),
 *   category = @Translation("External"),
 *   description = @Translation("Send the submission data to Maropost."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class MaropostWebformHandler extends WebformHandlerBase {

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * The module configuration object.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * Guzzle http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\ResettableStackedRouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->tokenManager = $container->get('webform.token_manager');
    $instance->settings = $container->get('config.factory')->get('maropost_sub_webform_handler.settings');
    $instance->client = $container->get('http_client');
    $instance->routeMatch = $container->get('current_route_match');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'subscription_id' => '',
      'email_token' => '',
      'first_name_token' => '',
      'last_name_token' => '',
      'custom_field_tokens' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {

    $form['configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Specific settings'),
    ];

    $form['configuration']['subscription_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subscription ID'),
      '#description' => $this->t('Marpost ID, if there are more than one, separate it with a comma. Example: 33,4,6'),
      '#default_value' => $this->configuration['subscription_id'],
    ];

    $form['configuration']['email_token'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Set the field token that will store the email'),
      '#title' => $this->t('Email token'),
      '#default_value' => $this->configuration['email_token'],
      '#required' => TRUE,
    ];

    $form['configuration']['first_name_token'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Set the field token that will store the first name'),
      '#title' => $this->t('First name token'),
      '#default_value' => $this->configuration['first_name_token'],
    ];

    $form['configuration']['last_name_token'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Set the field token that will store the last name'),
      '#title' => $this->t('Last name token'),
      '#default_value' => $this->configuration['last_name_token'],
    ];

    $form['configuration']['custom_field_tokens'] = [
      '#type' => 'textarea',
      '#description' => $this->t('Additional custom fields provided as "key|placeholder", one per row'),
      '#title' => $this->t('Custom field tokens'),
      '#default_value' => $this->configuration['custom_field_tokens'],
    ];

    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debugging'),
      '#description' => $this->t('If checked, every relevant item managed by the plugin will be serialized and shown to the user.'),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['debug'],
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['subscription_id'] = $form_state->getValue('subscription_id');
    $this->configuration['email_token'] = $form_state->getValue('email_token');
    $this->configuration['first_name_token'] = $form_state->getValue('first_name_token');
    $this->configuration['last_name_token'] = $form_state->getValue('last_name_token');
    $this->configuration['custom_field_tokens'] = $form_state->getValue('custom_field_tokens');
    $this->configuration['debug'] = $form_state->getValue('debug');
  }

  /**
   * {@inheritdoc}
   */
  public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $email = $this->getEmailValue();
    $this->debug('email', $email);
    $first_name = $this->getFirstNameValue();
    $this->debug('first_name', $first_name);
    $last_name = $this->getLastNameValue();
    $this->debug('last_name', $last_name);
    $custom_field_tokens = $this->getCustomFieldTokensValue();
    $this->debug('custom_field_tokens', $custom_field_tokens);
    $payload = self::getRequestPayload($email, $first_name, $last_name, $custom_field_tokens, $this->configuration['subscription_id']);
    $this->debug('payload', $payload);
    try {
      $response = $this->sendRequest($payload);
      $this->debug('response', $response);
    }
    catch (ClientException | GuzzleException $e) {
      $this->debug('exception', $e->getMessage());
      $this->getLogger('maropost_sub_webform_handler')->error($e->getMessage());
      $this->messenger()->addError($this->getFailueMessage());
      return FALSE;
    }

    $status = $this->parseResponse((string) $response->getBody());
    $this->debug('status', $status);
    if (!$status) {
      // @todo Fix to use $form_state->setRedirect.
      $redirect = new RedirectResponse(Url::fromRoute(
        $this->routeMatch->getRouteName(),
        $this->routeMatch->getParameters()->all()
      )->toString());
      $redirect->send();
    }
  }

  /**
   * Transform the email token into a user input value.
   *
   * @return string
   *   The field token from the configured value
   *
   * @throws \Exception
   *   See $this->getWebformSubmission() for details.
   */
  private function getEmailValue(): string {
    return $this->tokenManager->replaceNoRenderContext($this->configuration['email_token'], $this->getWebformSubmission());
  }

  /**
   * Transform the first name token into a user input value.
   *
   * @return string
   *   The field token from the configured value
   *
   * @throws \Exception
   *   See $this->getWebformSubmission() for details.
   */
  private function getFirstNameValue(): string {
    return $this->tokenManager->replaceNoRenderContext($this->configuration['first_name_token'], $this->getWebformSubmission());
  }

  /**
   * Transform the last name token into a user input value.
   *
   * @return string
   *   The field token from the configured value
   *
   * @throws \Exception
   *   See $this->getWebformSubmission() for details.
   */
  private function getLastNameValue(): string {
    return $this->tokenManager->replaceNoRenderContext($this->configuration['last_name_token'], $this->getWebformSubmission());
  }

  /**
   * Transform the custom field tokens into an array of key => processed values.
   *
   * @return array
   *   The field token from the configured value
   *
   * @throws \Exception
   *   See $this->getWebformSubmission() for details.
   */
  private function getCustomFieldTokensValue(): array {
    $replaced_values = $this->tokenManager->replaceNoRenderContext($this->configuration['custom_field_tokens'], $this->getWebformSubmission());
    $replaced_lines = preg_split("/\r\n|\n|\r/", $replaced_values);
    $mapped_keys = [];
    foreach ($replaced_lines as $single_line) {
      list($key, $value) = explode('|', $single_line);
      $mapped_keys[$key] = $value;
    }
    return $mapped_keys;
  }

  /**
   * Get the payload to be sent to Maropost endpoint.
   *
   * @param string $email
   *   The email to send to the remote api.
   * @param string $first_name
   *   The first name to send to the remote api.
   * @param string $last_name
   *   The last name to send to the remote api.
   * @param array $custom_field_tokens
   *   An array of custom fields to send to the remote api.
   * @param string $subscription_id
   *   The id of the service where to subscribe the user.
   *
   * @return string
   *   The payload ready to be sent.
   */
  private static function getRequestPayload(string $email, string $first_name, string $last_name, array $custom_field_tokens, string $subscription_id): string {
    $data = [];
    $data['contact']['email'] = $email;
    if (!empty($first_name)) {
      $data['contact']['first_name'] = $first_name;
    }
    if (!empty($last_name)) {
      $data['contact']['last_name'] = $last_name;
    }
    if (!empty($custom_field_tokens)) {
      $data['contact']['custom_field'] = $custom_field_tokens;
    }
    $data['contact']['options']['subscribe_list_ids'] = $subscription_id;
    return Json::encode($data);
  }

  /**
   * Send the given payload to the configured url.
   *
   * @param string $payload
   *   The payload of the request.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response from the WS.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   In case of unsuccessful request.
   */
  private function sendRequest(string $payload) {
    $this->debug('url', $this->settings->get('url'));
    $this->debug('api_key', $this->settings->get('api_key'));
    return $this->client->request(
      'POST',
      $this->settings->get('url'),
      [
        'query' => [
          'auth_token' => $this->settings->get('api_key'),
        ],
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'body' => $payload,
      ]
    );
  }

  /**
   * Return the public failure message.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translated message.
   */
  public function getFailueMessage() {
    return $this->t('Unable to subscribe to Maropost. Please contact the administration.');
  }

  /**
   * Log and show errors in case of error, parsing the response body.
   *
   * @param string $response
   *   The body of the WS response, as string.
   *
   * @return bool
   *   TRUE in case of successful subscription, FALSE otherwise.
   */
  private function parseResponse(string $response) {
    $body = JSON::decode($response);
    if (!isset($body->error)) {
      return TRUE;
    }
    $this->getLogger('maropost_sub_webform_handler')->error('Could not create Maropost contact: @message', ['@message' => $body->error]);
    $this->messenger()->addError($this->getFailueMessage());
    return FALSE;
  }

  /**
   * Display the debugging values.
   *
   * @param string $item_name
   *   The invoked method name.
   * @param mixed $item
   *   The item to be logged.
   */
  protected function debug(string $item_name, $item = NULL) {
    if (!empty($this->configuration['debug'])) {
      $t_args = [
        '@item_name' => $item_name,
        '@item_value' => is_scalar($item) ? $item : JSON::encode($item),
      ];
      $this->messenger()->addWarning($this->t('@item_name : @item_value', $t_args), TRUE);
    }
  }

}
