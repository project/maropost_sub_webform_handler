<?php

namespace Drupal\maropost_sub_webform_handler\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Maropost integration settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'maropost_sub_webform_handler_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['maropost_sub_webform_handler.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Auth Token'),
      '#required' => TRUE,
      '#default_value' => $this->config('maropost_sub_webform_handler.settings')->get('api_token'),
    ];
    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Url'),
      '#required' => TRUE,
      '#default_value' => $this->config('maropost_sub_webform_handler.settings')->get('url'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('maropost_sub_webform_handler.settings')
      ->set('api_token', $form_state->getValue('api_token'))
      ->set('url', $form_state->getValue('url'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
